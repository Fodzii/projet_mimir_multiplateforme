import { Component } from '@angular/core';
import { AlertController, NavController, Platform } from 'ionic-angular';
import { HomePage } from '../../home/home';
import { FiftyPage } from '../fifty/fifty';
import { TranslateService } from 'ng2-translate';
import { RiddleModel } from '../../../model/riddle';

import {Validators, FormBuilder, FormGroup } from '@angular/forms';


@Component({
  selector: 'page-tictac-ionic',
  templateUrl: 'tictac.html'
})

export class TicTacPage {
  private todo : FormGroup;
  private riddleModel;
  private tabCode = ['*','*','*','*'];

  constructor(public navCtrl : NavController, private formBuilder: FormBuilder, private alertCtrl: AlertController, private platform: Platform, public translate: TranslateService) {

    this.todo = this.formBuilder.group({
      reponse: ['', Validators.required],
    });
    this.riddleModel = new RiddleModel(platform, translate); 
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Low battery',
      subTitle: '10% of battery remaining',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  putCode(input:string){
    var changed = false;
    for (var _i = 0; _i < this.tabCode.length; _i++) {
      if (this.tabCode[_i] == '*' && changed == false) {
        this.tabCode[_i] = input; 
        changed = true;
      }
    }
  }

  clearCode(){
      this.tabCode = ['*','*','*','*'];
  }

  validerCode(){
    var codeResult : String = "";
    this.tabCode.forEach((item, index) => {
      codeResult += item.toString();
    });
    this.checkCode(codeResult);
  }

  checkCode(aCodeResult : String){
    var date = new Date();
    var valeur = date.getHours() + (date.getMinutes()<10?'0':'')+date.getMinutes();

    if(aCodeResult == valeur){
        this.riddleModel.setIsUnlockTrueByNumber(2);
        let alert = this.alertCtrl.create({
          title: 'Félicitation !',
          subTitle: 'Votre réponse est correcte',
          buttons: [{text:'Accueil', handler: () => {

            this.navCtrl.setRoot(HomePage);
          }},{text:'Engime suivante', handler: () => {

            this.navCtrl.push(FiftyPage);
          }}]

        });
        alert.present();
    } else {
      this.tabCode = ['*','*','*','*'];
      let alert = this.alertCtrl.create({
        title: 'Zut !',
        subTitle: 'Votre réponse n\'est pas correcte',
        buttons: [{text:'Accueil', handler: () => {

          this.navCtrl.setRoot(HomePage);
        }},{text:'Recommencer'}]

      });
      alert.present();
    }
  }

}