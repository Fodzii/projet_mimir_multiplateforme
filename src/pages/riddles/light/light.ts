import { Component } from '@angular/core';
import { AlertController, NavController, Platform } from 'ionic-angular'; 
import { TranslateService } from 'ng2-translate';

import { BatteryStatus } from 'ionic-native';
import { RiddleModel } from '../../../model/riddle';

import { HomePage } from '../../home/home';
import { WhisperPage }from '../whisper/whisper';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'page-light-ionic',
  templateUrl: 'light.html'
})
export class LightPage {

  public level: number;  
  public isPlugged: boolean;  
  private riddleModel;
  private imgSrc;

  status:any;
  Level:any = 0;


  constructor(public platform: Platform, public navCtrl : NavController, private formBuilder: FormBuilder, private alertCtrl: AlertController, public translate: TranslateService) {
    this.riddleModel = new RiddleModel(platform, translate);
    this.imgSrc = "assets/images/Light/Light_off.png";
    let subscription = BatteryStatus.onChange().subscribe(
    (status) => {
      this.Level = status.level;
      if(status.isPlugged) {
        this.imgSrc = "assets/images/Light/Light_on.png";
        subscription.unsubscribe();
        this.resolvedRiddle();
      }
    });
  }

  resolvedRiddle(){
    this.riddleModel.setIsUnlockTrueByNumber(4);
    let alert = this.alertCtrl.create({
      title: 'Félicitation !',
      subTitle: 'Votre réponse est correcte',
      buttons: [{text:'Accueil', handler: () => {                    
        this.navCtrl.setRoot(HomePage);
      }},{text:'Engime suivante', handler: () => {
        this.navCtrl.push(WhisperPage);
      }}]
    });

    setTimeout(() => {
      alert.present();
    }, 2000);

  }
}

