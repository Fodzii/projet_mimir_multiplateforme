import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

import { TranslateService } from 'ng2-translate';

import { RiddleModel } from '../../model/riddle';

import { SettingPage } from '../setting/setting';
import { FiftyPage } from '../riddles/fifty/fifty';
import { TicTacPage } from '../riddles/tictac/tictac';
import { LightPage } from '../riddles/light/light';
import { WhisperPage }from '../riddles/whisper/whisper';

@Component({
  selector: 'page-riddles-list',
  templateUrl: 'riddles-list.html'
})
export class RiddlesListPage {

  private riddleModel;
  
  private Pages = {
    'FiftyPage' : FiftyPage,
    'TicTacPage' : TicTacPage,
    'LightPage' : LightPage,
    'WhisperPage' : WhisperPage
  }

  selectedItem: any;
  SettingPage : any = SettingPage;
  icons: string[];
  items: Array<{title: string, note: string}>;
  isenabled:boolean=false;

  constructor(public navCtrl: NavController, public navParams: NavParams,  private platform: Platform, public translate: TranslateService) {

    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    this.riddleModel = new RiddleModel(platform, translate); 
  }

  itemTapped(event, item) {
    if(item.isUnlock == 1){
      this.navCtrl.push(this.Pages[item.namePage]);
    } else {
      alert('Vous n\' avez pas débloqué cette enigme');
    }
  }

    ionViewDidEnter(){
      /*this.riddleModel.riddles.forEach((item, index) => {
        if(item.isUnlock == 1){
          document.getElementById('button')[index].disabled = false;
        }
    });*/
  }
}
