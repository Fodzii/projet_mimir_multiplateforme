import {SQLite} from "ionic-native";

import { Platform } from 'ionic-angular';

import { TranslateService } from 'ng2-translate';

export class RiddleModel {

  public database: SQLite;
  public translatee: TranslateService;

  private number;
  private name;
  private namePage;
  private isUnlock;

  public riddles: Array<Object>;
  public riddle: Object;

  constructor(private platform: Platform, public translate: TranslateService) {
            platform.ready().then(() => {
            this.translatee = translate;
            this.database = new SQLite();
            this.database.openDatabase({
                name: "data.db",
                location: "default"
            }).then(() => {
                this.database.executeSql("CREATE TABLE IF NOT EXISTS riddles (number INTEGER PRIMARY KEY, name TEXT, namePage TEXT, isUnlock INTEGER)", {}).then((data) => {
                    this.initBDValue();
                    this.displayRiddleList();
                    console.log("TABLE CREATED: ", data);
                }, (error) => {
                    console.error("Unable to execute sql", error);
                })
            }, (error) => {
                console.error("Unable to open database", error);
            });
        });
   }

       public initBDValue() {
        this.database.executeSql("INSERT INTO riddles VALUES (1, '♫ Tic Tac, tourne l heure ♫', 'TicTacPage', 1)", []).then((data) => {
            console.log("INSERTED: " + JSON.stringify(data));
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error.err));
        });
        this.database.executeSql("INSERT INTO riddles VALUES (2, 'Fifty-fifty', 'FiftyPage', 0)", []).then((data) => {
            console.log("INSERTED: " + JSON.stringify(data));
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error.err));
        });
        this.database.executeSql("INSERT INTO riddles VALUES (3, 'Lumière !', 'LightPage', 0)", []).then((data) => {
            console.log("INSERTED: " + JSON.stringify(data));
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error.err));
        });
        this.database.executeSql("INSERT INTO riddles VALUES (4, 'Murmure', 'WhisperPage', 0)", []).then((data) => {
            console.log("INSERTED: " + JSON.stringify(data));
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error.err));
        });
    }
 
    public displayRiddleList() {
        this.database.executeSql("SELECT * FROM riddles", []).then((data) => {
            this.riddles = [];
            if(data.rows.length > 0) {
                for(var i = 0; i < data.rows.length; i++) {
                    this.number = data.rows.item(i).number; 
                    this.name = data.rows.item(i).name; 
                    this.namePage = data.rows.item(i).namePage;
                    this.isUnlock = data.rows.item(i).isUnlock;
                    this.translatee.get(this.name).subscribe(res => {
                      this.name = res;
                    });
                    this.riddles.push({number: this.number, name: this.name, namePage: this.namePage, isUnlock: this.isUnlock});
                }
            }
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error));
        });
    }

    public getFirstUnlockRiddle() {
        this.database.executeSql("SELECT * FROM riddles where isUnlock = 1 ORDER BY number DESC LIMIT 1;", []).then((data) => {
            if(data.rows.length > 0) {
                for(var i = 0; i < data.rows.length; i++) {
                    this.number = data.rows.item(i).number; 
                    this.name = data.rows.item(i).name; 
                    this.namePage = data.rows.item(i).namePage;
                    this.isUnlock = data.rows.item(i).isUnlock;
                }
            }
            this.riddle = ({number: this.number, name: this.name, namePage: this.namePage, isUnlock: this.isUnlock});
        }, (error) => {
            console.log("ERROR: " + JSON.stringify(error));
        });
    }

    public setIsUnlockTrueByNumber(number) {
      let sql = "UPDATE riddles SET isUnlock = 1 WHERE number = " + number;
      this.database.executeSql(sql, {})
      .then(() => console.log('Executed SQL'))
      .catch(e => console.log(e));
    }

}
